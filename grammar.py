
from happytransformer import  HappyTextToText
from happytransformer import TTSettings
import pandas as pd


happy_tt = HappyTextToText("T5", "vennify/t5-base-grammar-correction")
beam_settings =  TTSettings(num_beams=5, min_length=1, max_length=100)

df1=pd.read_csv("review_data.csv")

print(df1.isnull().sum())
df1.drop_duplicates(keep='first',inplace=True)
df1.dropna(axis=0,inplace=True)
print(df1.isnull().sum())

influent_sentences =list(df1['text'].head(17))
res=list(influent_sentences)
for influent_sentence in influent_sentences:
    output_text_1 = happy_tt.generate_text(influent_sentence, args=beam_settings)
    res=output_text_1.text
    print(res)



  