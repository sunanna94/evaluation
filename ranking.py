#imported required libraries

import pandas as pd
import seaborn as sns

#loading dataset

df=pd.read_csv('browser_rankings_data.csv')
df1=df.copy()

#checking for null values and reving duplicates and cleaning

df1.isnull().sum()
df1.drop_duplicates(keep='first',inplace=True)
df1.dropna(axis=0,inplace=True)

#To dispaly all columns

pd.set_option('display.max_columns', 500)
df1.describe()

#Converting to categorical variables and finding the correlation and visualize in heatmap

df1['Short Description']=df1['Short Description'].astype('category').cat.codes
df1['Keyword']=df['Keyword'].astype('category').cat.codes
df1['Long Description']=df1['Long Description'].astype('category').cat.codes
df1['App ID']=df1['App ID'].astype('category').cat.codes
p=df1.corr(method="pearson")
sns.heatmap(p)

#AppID Vs Rank

x=df1['App ID'].corr(df1['Rank'])
z=df1['Short Description'].corr(df1['Rank'])

#sns.barplot(x=df1['Rank'],y=df1['App ID'])

#keyword Vs Rank

crossAPP = pd.crosstab(index = df1["Keyword"],columns = df1['Rank'], margins = True, normalize =  'index') 
print(crossAPP)
y=(df1['Keyword'].head(10)).corr(df1['Rank'])








