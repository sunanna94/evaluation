#importing libraries

import streamlit as st
import pandas as pd
from nltk.sentiment.vader import SentimentIntensityAnalyzer

#App Header

st.header("The Non Matching Reviews comparing with Ratings in the dataset")

#loading dataset

chrome=pd.read_csv('chrome_reviews.csv')
chrome1=chrome.copy()

#Extracting required columns

chrome2=pd.DataFrame(data=chrome1,columns=['ID','Text','Star'])

#Removing null values and drop duplicates if any

chrome2.drop_duplicates(keep='first',inplace=True)
chrome2.dropna(axis=0,inplace=True)
    
#finding polarity scores using Sentiment INtensity Analyzer

txt=list(chrome2['Text'])
ratinglist = list(chrome2['Star'])
sentiments = SentimentIntensityAnalyzer()
chrome2["Positive"] = [sentiments.polarity_scores(i)["pos"] for i in chrome2["Text"]]
chrome2["Negative"] = [sentiments.polarity_scores(i)["neg"] for i in chrome2["Text"]]
chrome2["Neutral"] = [sentiments.polarity_scores(i)["neu"] for i in chrome2["Text"]]

#for printing not matching reviews

res=list(txt)
i=0
for data in txt:
   i=i+1
   ss=sentiments.polarity_scores(data)
   if ss['compound']>0.5 and ratinglist[i]<3:
      res=''.join(data)
      st.write(res,ratinglist[i])   
      


