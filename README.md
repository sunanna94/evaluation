# evaluation

EVALUATION -1 ANSWERS

 PART 1
 
 Q1. File name -- "noextract.py"
 Q2. File name -- "nonmatching.py"
 Q3. File name -- "ranking.py"
 
 PART 2

 Q1. File name -- "grammar.py"

 MORE BONUS POINTS

 1)  Dataset handling and finding the best prediction algorithm for my final year project was very difficult task for me at that time. But, when I learned about it, its pretty interesting. 

 2)  The set of pairs (a, 2a) for all real a is a subspace of V
    Since,
    (0,0) E V'
    (v1,v2) E V'
    v1+v2 E V'
    cv1 E V' 

 PART 1
 
 Q1. To extract the numbers from dictionary (noextract.py)
     
     --> Import the library "re" for writing Regex
     --> Assign dictionary to a variable
     --> Convert it to string for performing extraction
     --> Loop to traverse along all values and for finding all digits I used "/d+" and findall() in re
     --> Print 
     
 Q2. To identify low rating, but good comments and deploy it in Streamlit(nonmatching.py) 
     
     --> Used "Pandas" for data loading and preprocessing
     --> Used "Vader Sentiment Intensity Analyzer" for getting the polarity scores of each reviews
     --> Used "Streamlit" for deploying
     **** Network URL - http://192.168.0.109:8501 ****
 
 Q3. To findout the correlation between each attributes in the dataset(ranking.py)
     
     --> Used "Pandas" for data loading and preprocessing
     --> Used "Seaborn" for data visualization
     
     Suggested Questions
     
     1) There exists negative correlation between Keyword,Long description, Short Description and Rank columns
     2) There exists a positive correlation of about 0.5 between App ID and Rank columns
     3) If we used a classifier for predicting the Rank based on the AppID and description it will be helpful for the users to find better browsers
     
  PART 2
  
  Q1.  To check the review in the dataset is grammatically correct
  
       --> Used happytransformer for grammar correction
       --> accuracy checking of sentences import sklearn.metrics   
        
     
  THANK YOU    
          
   
